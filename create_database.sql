--
-- Table structure for table `handset`
--
CREATE TABLE IF NOT EXISTS `handset` (
  `id`               INT(11) NOT NULL AUTO_INCREMENT,
  `mac_address`      VARCHAR(12)      DEFAULT NULL,
  `call_server_type` VARCHAR(10)      DEFAULT NULL,
  `connect_server`   VARCHAR(10)      DEFAULT NULL,
  `node_id`          VARCHAR(4)       DEFAULT NULL,
  `terminal_number`  VARCHAR(12)      DEFAULT NULL,
  `comment`          VARCHAR(50)      DEFAULT NULL,
  `config_type_id`   INT(11)          DEFAULT 0,
  `created`          datetime         DEFAULT NULL,
  `created_by`       VARCHAR(40)      DEFAULT NULL,
  `modified`         datetime         DEFAULT NULL,
  `modified_by`      VARCHAR(40)      DEFAULT NULL,
  PRIMARY KEY (`id`)
);
--
-- Table structure for table `settings`
--
CREATE TABLE IF NOT EXISTS `settings` (
  `id`             INT(11) NOT NULL AUTO_INCREMENT,
  `config_code`    VARCHAR(15) NOT NULL,
  `config_value`   VARCHAR(25),
  `comment`        VARCHAR(50),
  `config_type_id` INT(11) DEFAULT 0,
  PRIMARY KEY (`id`)
);
--
-- Table structure for table `config_type`
--
CREATE TABLE IF NOT EXISTS `config_type` (
  `id`          INT(11) NOT NULL,
  `description` VARCHAR(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
);
--
-- Table Structure for table `node`
--
CREATE TABLE IF NOT EXISTS `node` (
  `id`          VARCHAR(4) NOT NULL,
  `description` VARCHAR(50) DEFAULT "",
  PRIMARY KEY (`id`)
);
--
-- Table structure for Groups and Users
--
CREATE TABLE IF NOT EXISTS `groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `firstname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lastname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `department` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` char(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `group_id` int(11) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `log_requests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `filename` varchar(50) DEFAULT NULL,
  `ip_address` INT UNSIGNED DEFAULT NULL,
  `handset_id` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Populate the default config_types
--
-- INSERT INTO `config_type` VALUE ('0', 'Default'),
--  ('1', 'Wilmington Campus'),
--  ('2', 'North Campus'),
--  ('3', 'Burgaw Campus'),
--  ('4', 'Surf City Campus'),
--  ('100', 'System Settings');
--
-- Populate the settings with defaults
--
-- INSERT INTO `settings` (config_code, config_value, comment, config_type_id) VALUES
--  ('file', 'd', 'Read Device File', 100),
--  ('menulock', 'u', 'Menu lock mode', 0),
--  ('vq', 'n', 'Enable 802.1Q for voice', 0),
--  ('pc', 'y', 'Enable PC port', 0),
--  ('pcs', 'a', 'PC port speed', 0),
--  ('pcd', 'a', 'PC port duplex', 0),
--  ('dq', 'n', 'Enable 802.1Q for PC port', 0),
--  ('lldp', 'n', 'Enable 802.1ab (LLDP)', 0),
--  ('stickiness', 'y', 'Enable stickiness', 0),
--  ('cachedip', 'y', 'Enable cached IP', 0),
--  ('s1ip', '192.168.7.10', 'Primary server IP address', 0),
--  ('p1', '4100', 'Primary server port number', 0),
--  ('a1', '1', 'Primary server action code', 0),
--  ('r1', '2', 'Primary server retry count', 0),
--  ('s2ip', '192.168.7.10', 'Secondary server IP address', 0),
--  ('p2', '4100', 'Secondary server port number', 0),
--  ('a2', '1', 'Secondary server action code', 0),
--  ('r2', '2', 'Secondary server retry count', 0),
--  ('th', '5', 'Theme', 0);

INSERT INTO `node` (id, description) VALUES ('0001', 'Default Node');
