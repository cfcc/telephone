# README #

This is a web application that functions as a provisioning server for Avaya CS1K PBX phones.
Originally Avaya only supported TFTP servers, but after a while could support HTTP connections as well.
The provisioning files are simply text, but this project let's one store the handset information in a database
and then generate the provisioning text files on the fly.

### Summary ###

* Avaya Handset Provisioning Server
* Version 2.0

Note that Version 1 used jQuery and is no longer being developed.  The code is still available in the version1 branch.

### Setup ###

This is a LAMP project and can normally be unzipped into the destination directory, or uploaded through the IDE.

A sample configuration for Apache is in the apache2.conf file.  The create_database.sql file has the tables as well as
some default data that can be used to create the basic handset settings.

The DHCP settings required for the provisioning server to work are very simple:

 * SOMETHING
 * SOMETHING ELSE  

#### Requirements ####
 * Apache 2.4 with SSL
 * MySQL
 
#### Dependencies ####
 * Uses the Cake 2.0 Framework (delivered in the project files)
 
#### Configuration ####
 * See: app/Config/database.php.default

### Author ###

* Jakim Friant <jfriant@cfcc.edu>
