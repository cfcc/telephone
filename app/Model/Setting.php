<?php
App::uses('AppModel', 'Model');
/**
 * Setting Model
 *
 */
class Setting extends AppModel {
    /**
     * Display field
     *
     * @var string
     */
    public $displayField = 'config_code';

    /**
     * belongsTo associations
     *
     * @var array
     */
    public $belongsTo = array (
        'ConfigType' => array (
            'className' => 'ConfigType',
            'foreignKey' => 'config_type_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
	);
}
