<?php
App::uses('AppModel', 'Model');
/**
 * Node Model
 *
 * @property Handset $Handset
 */
class Node extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'node';

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'description';


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Handset' => array(
			'className' => 'Handset',
			'foreignKey' => 'node_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
