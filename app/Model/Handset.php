<?php
App::uses('AppModel', 'Model');
/**
 * Handset Model
 *
 * @property Node $Node
 * @property ConfigType $ConfigType
 * @property LogRequest $LogRequest
 */
class Handset extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'handset';

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'mac_address';


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Node' => array(
			'className' => 'Node',
			'foreignKey' => 'node_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'ConfigType' => array(
			'className' => 'ConfigType',
			'foreignKey' => 'config_type_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'LogRequest' => array(
			'className' => 'LogRequest',
			'foreignKey' => 'handset_id',
			'order' => 'LogRequest.created DESC',
		)
	);
}
