<?php
App::uses('AppModel', 'Model');
/**
 * ConfigType Model
 *
 */
class ConfigType extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'config_type';

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'description';

}
