<div class="handsets view">
<h2><?php echo __('Handset'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($handset['Handset']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Mac Address'); ?></dt>
		<dd>
			<?php echo h($handset['Handset']['mac_address']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Call Server Type'); ?></dt>
		<dd>
			<?php echo h($handset['Handset']['call_server_type']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Connect Server'); ?></dt>
		<dd>
			<?php echo h($handset['Handset']['connect_server']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Node Id'); ?></dt>
		<dd>
			<?php echo h($handset['Handset']['node_id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Terminal Number'); ?></dt>
		<dd>
			<?php echo h($handset['Handset']['terminal_number']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Extension'); ?></dt>
		<dd>
			<?php echo h($handset['Handset']['extension']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Config Type'); ?></dt>
		<dd>
			<?php echo $this->Html->link($handset['ConfigType']['description'], array('controller' => 'config_types', 'action' => 'view', $handset['ConfigType']['id'])); ?>
			&nbsp;
		</dd>
        <dt><?php echo __('Comment'); ?></dt>
        <dd>
            <?php echo h($handset['Handset']['comment']); ?>
            &nbsp;
        </dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($handset['Handset']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created By'); ?></dt>
		<dd>
			<?php echo h($handset['Handset']['created_by']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($handset['Handset']['modified']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified By'); ?></dt>
		<dd>
			<?php echo h($handset['Handset']['modified_by']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link("<i class='fas fa-edit'></i> " . __('Edit Handset'), array('action' => 'edit', $handset['Handset']['id']), array('escape' => false)); ?> </li>
		<li><?php echo $this->Form->postLink("<i class='fas fa-trash'></i> " . __('Delete Handset'), array('action' => 'delete', $handset['Handset']['id']), array('escape' => false), __('Are you sure you want to delete # %s?', $handset['Handset']['id'])); ?> </li>
        <li><?php echo $this->Html->link("<i class='fas fa-eye'></i> " . __('Preview'), array('controller' => 'provision', 'action' => 'get', $handset['Handset']['mac_address'] . '.prv'), array('escape' => false)); ?></li>
        <li><?php echo $this->Html->link("<i class='fas fa-phone'></i> " . __('List Handsets'), array('action' => 'index'), array('escape' => false)); ?> </li>
		<li><?php echo $this->Html->link("<i class='fas fa-plus'></i> " . __('New Handset'), array('action' => 'add'), array('escape' => false)); ?> </li>
		<li><?php echo $this->Html->link("<i class='fas fa-globe'></i> " . __('List Config Types'), array('controller' => 'config_types', 'action' => 'index'), array('escape' => false)); ?> </li>
		<li><?php echo $this->Html->link("<i class='fas fa-plus-circle'></i> " . __('New Config Type'), array('controller' => 'config_types', 'action' => 'add'), array('escape' => false)); ?> </li>
		<li><?php echo $this->Html->link("<i class='fas fa-list'></i> " . __('List Log Requests'), array('controller' => 'log_requests', 'action' => 'index'), array('escape' => false)); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Log Requests'); ?></h3>
	<?php if (!empty($handset['LogRequest'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Filename'); ?></th>
		<th><?php echo __('Ip Address'); ?></th>
		<th><?php echo __('Handset Id'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
	</tr>
	<?php foreach ($handset['LogRequest'] as $logRequest): ?>
		<tr>
			<td><?php echo $logRequest['id']; ?></td>
			<td><?php echo $logRequest['filename']; ?></td>
			<td><?php echo long2ip($logRequest['ip_address']); ?></td>
			<td><?php echo $logRequest['handset_id']; ?></td>
			<td><?php echo $logRequest['created']; ?></td>
			<td><?php echo $logRequest['modified']; ?></td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>
</div>
