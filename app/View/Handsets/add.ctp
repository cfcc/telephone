<div class="handsets form">
<?php echo $this->Form->create('Handset'); ?>
	<fieldset>
		<legend><?php echo __('Add Handset'); ?></legend>
	<?php
		echo $this->Form->input('mac_address');
		echo $this->Form->input('call_server_type', array('default' => 'CS1K'));
		echo $this->Form->input('connect_server', array('default' => 'S1S2'));
		echo $this->Form->input('node_id');
		echo $this->Form->input('terminal_number');
		echo $this->Form->input('extension');
		echo $this->Form->input('config_type_id');
        echo $this->Form->input('comment');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
        <li><?php echo $this->Html->link("<i class='fas fa-phone'></i> " . __('List Handsets'), array('controller' => 'handsets', 'action' => 'index'), array('escape' => false)); ?> </li>
	</ul>
</div>
