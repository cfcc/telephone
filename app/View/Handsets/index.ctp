<div class="handsets index">
	<h2><?php echo __('Handsets'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('mac_address'); ?></th>
            <?php /*
			<th><?php echo $this->Paginator->sort('call_server_type'); ?></th>
			<th><?php echo $this->Paginator->sort('connect_server'); ?></th>
			<th><?php echo $this->Paginator->sort('node_id'); ?></th>
 */ ?>
			<th><?php echo $this->Paginator->sort('terminal_number'); ?></th>
			<th><?php echo $this->Paginator->sort('extension'); ?></th>
			<th><?php echo $this->Paginator->sort('config_type_id'); ?></th>
            <th><?php echo $this->Paginator->sort('comment'); ?></th>
            <th><?php echo $this->Paginator->sort('ip_address'); ?></th>
			<th><?php echo $this->Paginator->sort('modified'); ?></th>
			<th><?php echo $this->Paginator->sort('modified_by'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($handsets as $handset):
        if (isset($handset['LogRequest'][0]['ip_address'])) {
            $ip_address = long2ip($handset['LogRequest'][0]['ip_address']);
        } else {
            $ip_address = "";
        }
        ?>
        <tr>
            <td><?php echo $this->Html->link($handset['Handset']['id'], array('action' => 'view', $handset['Handset']['id'])); ?>&nbsp;</td>
            <td><?php echo h($handset['Handset']['mac_address']); ?>&nbsp;</td>
            <?php /*
 <td><?php echo h($handset['Handset']['call_server_type']); ?>&nbsp;</td>
 <td><?php echo h($handset['Handset']['connect_server']); ?>&nbsp;</td>
            <td><?php echo h($handset['Handset']['node_id']); ?>&nbsp;</td>
 */ ?>
            <td><?php echo h($handset['Handset']['terminal_number']); ?>&nbsp;</td>
            <td><?php echo h($handset['Handset']['extension']); ?>&nbsp;</td>
            <td>
                <?php echo $this->Html->link($handset['ConfigType']['description'], array('controller' => 'config_types', 'action' => 'view', $handset['ConfigType']['id'])); ?>
            </td>
            <td><?php echo h($handset['Handset']['comment']); ?>&nbsp;</td>
            <td><?php echo h($ip_address); ?> </td>
            <td><?php echo h($handset['Handset']['modified']); ?>&nbsp;</td>
            <td><?php echo h($handset['Handset']['modified_by']); ?>&nbsp;</td>
            <td class="actions">
                <?php echo $this->Html->link("<i class='fas fa-edit'></i> " . __('Edit'), array('action' => 'edit', $handset['Handset']['id']), array('escape' => false)); ?>
                <?php echo $this->Form->postLink("<i class='fas fa-trash'></i> " . __('Delete'), array('action' => 'delete', $handset['Handset']['id']), array('escape' => false), __('Are you sure you want to delete # %s?', $handset['Handset']['id'])); ?>
            </td>
        </tr>
    <?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
        echo $this->Paginator->first('<< ' . __('first'));
		echo $this->Paginator->numbers(array('separator' => ''));
        echo $this->Paginator->last(__('last') . ' >>');
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link("<i class='fas fa-plus'></i> " . __('New Handset'), array('action' => 'add'), array('escape' => false)); ?></li>
        <li><?php echo $this->Html->link("<i class='fas fa-cog'></i> " . __('List Settings'), array('controller' => 'settings', 'action' => 'index'), array('escape' => false)); ?> </li>
		<li><?php echo $this->Html->link("<i class='fas fa-globe'></i> " . __('List Config Types'), array('controller' => 'config_types', 'action' => 'index'), array('escape' => false)); ?> </li>
		<li><?php echo $this->Html->link("<i class='fas fa-list-alt'></i> " . __('List Log Requests'), array('controller' => 'log_requests', 'action' => 'index'), array('escape' => false)); ?> </li>
        <li><?php echo $this->Html->link("<i class='fas fa-sign-out-alt'></i> " . __('Logout'), array('controller' => 'users', 'action' => 'logout'), array('escape' => false)); ?></li>
	</ul>
</div>
