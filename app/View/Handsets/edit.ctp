<div class="handsets form">
<?php echo $this->Form->create('Handset'); ?>
	<fieldset>
		<legend><?php echo __('Edit Handset'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('mac_address');
		echo $this->Form->input('call_server_type');
		echo $this->Form->input('connect_server');
		echo $this->Form->input('node_id');
		echo $this->Form->input('terminal_number');
        echo $this->Form->input('extension');
		echo $this->Form->input('config_type_id');
        echo $this->Form->input('comment');
		echo $this->Form->hidden('created_by');
		echo $this->Form->hidden('modified_by');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink("<i class='fas fa-trash'></i> " . __('Delete'), array('action' => 'delete', $this->Form->value('Handset.id')), array('escape' => false), __('Are you sure you want to delete # %s?', $this->Form->value('Handset.id'))); ?></li>
		<li><?php echo $this->Html->link("<i class='fas fa-phone'></i> " . __('List Handsets'), array('action' => 'index'), array('escape' => false)); ?></li>
	</ul>
</div>
