<div class="settings view">
<h2><?php echo __('Setting'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($setting['Setting']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Config Code'); ?></dt>
		<dd>
			<?php echo h($setting['Setting']['config_code']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Config Value'); ?></dt>
		<dd>
			<?php echo h($setting['Setting']['config_value']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Comment'); ?></dt>
		<dd>
			<?php echo h($setting['Setting']['comment']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Config Type'); ?></dt>
		<dd>
			<?php echo $this->Html->link($setting['ConfigType']['description'], array('controller' => 'config_types', 'action' => 'view', $setting['ConfigType']['id'])); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Setting'), array('action' => 'edit', $setting['Setting']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Setting'), array('action' => 'delete', $setting['Setting']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $setting['Setting']['id']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Settings'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Setting'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Config Types'), array('controller' => 'config_types', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Config Type'), array('controller' => 'config_types', 'action' => 'add')); ?> </li>
	</ul>
</div>
