<div class="settings form">
<?php echo $this->Form->create('Setting'); ?>
	<fieldset>
		<legend><?php echo __('Add Setting'); ?></legend>
	<?php
		echo $this->Form->input('config_code');
		echo $this->Form->input('config_value');
		echo $this->Form->input('comment');
		echo $this->Form->input('config_type_id');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Settings'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Config Types'), array('controller' => 'config_types', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Config Type'), array('controller' => 'config_types', 'action' => 'add')); ?> </li>
	</ul>
</div>
