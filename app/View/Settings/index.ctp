<div class="settings index">
    <h2><?php echo __('Setting'); ?></h2>
    <table cellpadding="0" cellspacing="0">
        <thead>
        <tr>
            <th><?php echo $this->Paginator->sort('id'); ?></th>
            <th><?php echo $this->Paginator->sort('config_code'); ?></th>
            <th><?php echo $this->Paginator->sort('config_value'); ?></th>
            <th><?php echo $this->Paginator->sort('comment'); ?></th>
            <th><?php echo $this->Paginator->sort('config_type_id'); ?></th>
            <th class="actions"><?php echo __('Actions'); ?></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($settings as $record): ?>
        <tr>
            <td><?php echo h($record['Setting']['id']); ?></td>
            <td><?php echo h($record['Setting']['config_code']) ?></td>
            <td><?php echo h($record['Setting']['config_value']) ?></td>
            <td><?php echo h($record['Setting']['comment']) ?></td>
            <td>
                <?php echo $this->Html->link($record['ConfigType']['description'], array('controller' => 'config_types', 'action' => 'view', $record['ConfigType']['id'])); ?>
            </td>
            <td class="actions">
                <?php echo $this->Html->link("<i class='fas fa-edit'></i> " . __('Edit'), array('action' => 'edit', $record['Setting']['id']), array('escape' => false)); ?>
                <?php echo $this->Form->postLink("<i class='fas fa-trash'></i> " . __('Delete'), array('action' => 'delete', $record['Setting']['id']), array('escape' => false), __('Are you sure you want to delete # %s?', $record['Setting']['id'])); ?>
                <?php
                if ($record['Setting']['config_type_id'] != 0 && $record['Setting']['config_type_id'] != 99) {
                    echo $this->Html->link("<i class='fas fa-eye'></i> " . __('Preview'), array('controller' => 'provision', 'action' => 'get', $record['Setting']['config_type_id'] . ".cfg"), array('escape' => false));
                } else {
                    echo $this->Html->link("<span style='color: lightgray;'><i class='fas fa-eye'></i> " . __('Preview') . "</span>", "#", array('escape' => false));
                }
                ?>
            </td>
        </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
    <p>
        <?php
        echo $this->Paginator->counter(array(
            'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
        ));
        ?>
    </p>
    <div class="paging">
        <?php
        echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
        echo $this->Paginator->numbers(array('separator' => ''));
        echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
        ?>
    </div>
</div>
<div class="actions">
    <h3><?php echo __('Actions'); ?></h3>
    <ul>
        <li><?php echo $this->Html->link("<i class='fas fa-plus'></i> " . __('New Setting'), array('action' => 'add'), array('escape' => false)); ?></li>
        <li><?php echo $this->Html->link("<i class='fas fa-globe'></i> " . __('List Config Types'), array('controller' => 'config_types', 'action' => 'index'), array('escape' => false)); ?> </li>
        <li><?php echo $this->Html->link("<i class='fas fa-phone'></i> " . __('List Handsets'), array('controller' => 'handsets', 'action' => 'index'), array('escape' => false)); ?> </li>
    </ul>
</div>
