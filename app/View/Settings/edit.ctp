<div class="settings form">
<?php echo $this->Form->create('Setting'); ?>
	<fieldset>
		<legend><?php echo __('Edit Setting'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('config_code');
		echo $this->Form->input('config_value');
		echo $this->Form->input('comment');
		echo $this->Form->input('config_type_id');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink("<i class='fas fa-trash'></i> " . __('Delete'), array('action' => 'delete', $this->Form->value('Settings.id')), array('confirm' => __('Are you sure you want to delete # %s?', $this->Form->value('Settings.id')), 'escape' => false)); ?></li>
		<li><?php echo $this->Html->link("<i class='fas fa-cog'></i> " . __('List Settings'), array('action' => 'index'), array('escape' => false)); ?></li>
		<li><?php echo $this->Html->link("<i class='fas fa-globe'></i> " . __('List Config Types'), array('controller' => 'config_types', 'action' => 'index'), array('escape' => false)); ?> </li>
		<li><?php echo $this->Html->link("<i class='fas fa-plus'></i> " . __('New Config Type'), array('controller' => 'config_types', 'action' => 'add'), array('escape' => false)); ?> </li>
	</ul>
</div>
