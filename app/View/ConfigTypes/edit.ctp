<div class="configTypes form">
<?php echo $this->Form->create('ConfigType'); ?>
	<fieldset>
		<legend><?php echo __('Edit Config Type'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('description');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink("<i class='fas fa-trash'></i> " . __('Delete'), array('action' => 'delete', $this->Form->value('ConfigType.id')), array('escape' => false), __('Are you sure you want to delete # %s?', $this->Form->value('ConfigType.id'))); ?></li>
		<li><?php echo $this->Html->link("<i class='fas fa-globe'></i> " . __('List Config Types'), array('action' => 'index'), array('escape' => false)); ?></li>
	</ul>
</div>
