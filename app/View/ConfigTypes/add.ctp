<div class="configTypes form">
<?php echo $this->Form->create('ConfigType'); ?>
	<fieldset>
		<legend><?php echo __('Add Config Type'); ?></legend>
	<?php
        echo $this->Form->input('id', array(
                'label' => 'Config Type ID',
                'type' => 'text',
            ));
		echo $this->Form->input('description');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Config Types'), array('action' => 'index')); ?></li>
	</ul>
</div>
