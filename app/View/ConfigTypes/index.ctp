<div class="configTypes index">
	<h2><?php echo __('Config Types'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('description'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($configTypes as $configType): ?>
	<tr>
		<td><?php echo h($configType['ConfigType']['id']); ?>&nbsp;</td>
		<td><?php echo h($configType['ConfigType']['description']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link("<i class='fas fa-edit'></i> " . __('Edit'), array('action' => 'edit', $configType['ConfigType']['id']), array('escape' => false)); ?>
			<?php echo $this->Form->postLink("<i class='fas fa-trash'></i> " . __('Delete'), array('action' => 'delete', $configType['ConfigType']['id']), array('escape' => false), __('Are you sure you want to delete # %s?', $configType['ConfigType']['id'])); ?>
		</td>
	</tr>
    <?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link("<i class='fas fa-plus'></i> " . __('New Config Type'), array('action' => 'add'), array('escape' => false)); ?></li>
        <li><?php echo $this->Html->link("<i class='fas fa-cog'></i> " . __('List Settings'), array('controller' => 'settings', 'action' => 'index'), array('escape' => false)); ?> </li>
        <li><?php echo $this->Html->link("<i class='fas fa-phone'></i> " . __('List Handsets'), array('controller' => 'handsets', 'action' => 'index'), array('escape' => false)); ?> </li>
	</ul>
</div>
