<div class="configTypes view">
<h2><?php echo __('Config Type'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($configType['ConfigType']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Description'); ?></dt>
		<dd>
			<?php echo h($configType['ConfigType']['description']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Config Type'), array('action' => 'edit', $configType['ConfigType']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Config Type'), array('action' => 'delete', $configType['ConfigType']['id']), array(), __('Are you sure you want to delete # %s?', $configType['ConfigType']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Config Types'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Config Type'), array('action' => 'add')); ?> </li>
	</ul>
</div>
