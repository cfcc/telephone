<?php
// app/View/Handset/index.ctp
$this->Html->css(array(
		'easyui/themes/default/easyui',
		'easyui/themes/icon',
		'easyui/themes/color',
		'dialog'
), array('inline' => false));
$this->Html->script(array(
		'easyui/jquery.min',
		'easyui/jquery.easyui.min',
		'listing'
), array('inline' => false));

$this->start('topmenu');
echo $this->Html->link('Edit Global Config', array('controller' => 'settings'), array('class' => 'easyui-linkbutton', 'iconCls' => 'icon-16-gear')) . PHP_EOL;
echo $this->Html->link('Upload a Spreadsheet', array('controller' => 'upload'), array('class' => 'easyui-linkbutton', 'iconCls' => 'icon-16-upload')) . PHP_EOL;
$this->end();

$url_handset_get = Router::url(array('controller' => 'Handset', 'action' => 'get.json'));
$url_configtype_get = Router::url(array('controller' => 'ConfigType', 'action' => 'get.json'));
?>
<table id="dg"
       class="easyui-datagrid"
       style="width:1024px;height=600px"
       url="<?php echo $url_handset_get ?>"
       fitColumns="true" singleSelect="true"
       idField="id" toolbar="#toolbar">
<thead>
<tr>
    <th field="mac_address">MAC Addr</th>
    <th field="call_server_type">Call Server Type</th>
    <th field="connect_server">Connect Server</th>
    <th field="node_id">Node ID</th>
    <th field="terminal_number">TN</th>
    <th field="last_ip">Last IP</th>
    <th field="comment">Comments</th>
    <th field="config_type_id">Campus</th>
    <th field="modified">Modified</th>
    <th field="modified_by">Modified By</th>
</tr>
</thead>
</table>
<div id="toolbar">
    <a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="newEntry()">Add Entry</a>
    <a href="#" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onclick="editEntry()">Edit Entry</a>
    <a href="#" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="destroyEntry()">Remove Entry</a>
    <a href="#" class="easyui-linkbutton" iconCls="icon-16-preview" plain="true" onclick="previewEntry()">Preview</a>
</div>

<div id="dlg" class="easyui-dialog" style="width:480px;height:400px;padding:10px 20px;"
     closed="true"
     buttons="#dlg-buttons">
    <div class="ftitle">Handset Information</div>
    <form id="fm" method="post" novalidate>
        <input type="hidden" name="id" />
        <div class="fitem">
            <label>MAC Address:</label>
            <input name="mac_address" class="easyui-textbox" required="true">
        </div>
        <div class="fitem">
            <label>Call Server Type:</label>
            <input name="call_server_type" class="easyui-textbox">
        </div>
        <div class="fitem">
            <label>Connect Server:</label>
            <input name="connect_server" class="easyui-textbox">
        </div>
        <div class="fitem">
            <label>Node ID:</label>
            <input name="node_id" class="easyui-textbox">
        </div>
        <div class="fitem">
            <label>Terminal Number:</label>
            <input name="terminal_number" class="easyui-textbox" required="true">
        </div>
        <div class="fitem">
            <label>Comments:</label>
            <input name="comment" class="easyui-textbox">
        </div>
        <div class="fitem">
            <label>Config Type:</label>
            <input id="cc_config_type" name="config_type_id" class="easyui-combobox" data-options="valueField:'id',textField:'text',url:'<?php echo $url_configtype_get ?>'">
        </div>
    </form>
</div>
<div id="dlg-buttons">
    <a href="#" class="easyui-linkbutton c6" iconCls="icon-ok" onclick="saveEntry()" style="width:90px">Save</a>
    <a href="#" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg').dialog('close')" style="width:90px">Cancel</a>
</div>
