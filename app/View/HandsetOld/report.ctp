<?php
// app/View/Handset/index.ctp
$this->Html->css(array('cake.generic'), array('inline' => false));
?>

<h1>Handsets</h1>
<table>
<tr>
<th>Id</th>
<th>MAC Address</th>
<th>Terminal Number</th>
<th>Call Server Type</th>
<th>Connect Server</th>
<th>Node ID</th>
</tr>
  <?php foreach ($handsets as $handset): ?>
  <tr>
    <td><?php echo $handset['Handset']['id']; ?></td>
    <td><?php echo $handset['Handset']['mac_address']; ?></td>
    <td><?php echo $handset['Handset']['terminal_number']; ?></td>
    <td><?php echo $handset['Handset']['call_server_type']; ?></td>
    <td><?php echo $handset['Handset']['connect_server']; ?></td>
    <td><?php echo $handset['Handset']['node_id']; ?></td>
  </tr>
  <?php endforeach; ?>
  <?php unset($handset); ?>
</table>
