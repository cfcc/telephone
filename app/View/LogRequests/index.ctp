<div class="logRequests index">
	<h2><?php echo __('Log Requests'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('filename'); ?></th>
			<th><?php echo $this->Paginator->sort('ip_address'); ?></th>
			<th><?php echo $this->Paginator->sort('handset_id'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('modified'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($logRequests as $logRequest): ?>
	<tr>
		<td><?php echo h($logRequest['LogRequest']['filename']); ?>&nbsp;</td>
		<td><?php echo h(long2ip($logRequest['LogRequest']['ip_address'])); ?>&nbsp;</td>
		<td><?php echo h($logRequest['Handset']['id']); ?></td>
		<td><?php echo h($logRequest['LogRequest']['created']); ?>&nbsp;</td>
		<td><?php echo h($logRequest['LogRequest']['modified']); ?>&nbsp;</td>
		<td class="actions">
            <?php
            if (isset($logRequest['Handset']['id'])) {
                echo $this->Html->link("<i class='fas fa-phone'></i> " . __('Show'), array('controller' => 'handsets', 'action' => 'view', $logRequest['Handset']['id']), array('escape' => false));
            } else {
                echo $this->Html->link("<span style='color: lightgray;'><i class='fas fa-phone'></i> " . __('Show') . "</span>", "#", array('escape' => false));
            }
            ?>
            <?php echo $this->Form->postLink("<i class='fas fa-trash'></i> " . __('Delete'), array('action' => 'delete', $logRequest['LogRequest']['id']), array('escape' => false), __('Are you sure you want to delete # %s?', $logRequest['LogRequest']['id'])); ?>		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p><?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?></p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link("<i class='fas fa-phone'></i> " . __('List Handsets'), array('controller' => 'handsets', 'action' => 'index'), array('escape' => false)); ?> </li>
	</ul>
</div>
