<div class="logRequests view">
<h2><?php echo __('Log Request'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($logRequest['LogRequest']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Filename'); ?></dt>
		<dd>
			<?php echo h($logRequest['LogRequest']['filename']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Ip Address'); ?></dt>
		<dd>
			<?php echo h(long2ip($logRequest['LogRequest']['ip_address'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Handset'); ?></dt>
		<dd>
			<?php echo $this->Html->link($logRequest['Handset']['id'], array('controller' => 'handsets', 'action' => 'view', $logRequest['Handset']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($logRequest['LogRequest']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($logRequest['LogRequest']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Log Request'), array('action' => 'edit', $logRequest['LogRequest']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Log Request'), array('action' => 'delete', $logRequest['LogRequest']['id']), array(), __('Are you sure you want to delete # %s?', $logRequest['LogRequest']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Log Requests'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Log Request'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Handsets'), array('controller' => 'handsets', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Handset'), array('controller' => 'handsets', 'action' => 'add')); ?> </li>
	</ul>
</div>
