<?php
// app/View/Systemconfig/index.ctp
$this->Html->css(array(
    'easyui/themes/default/easyui',
    'easyui/themes/icon',
    'easyui/themes/color',
    'dialog'
), array('inline' => false));
$this->Html->script(array(
    'easyui/jquery.min',
    'easyui/jquery.easyui.min',
    'easyui/extension/jquery.edatagrid',
    'sysconf'
), array('inline' => false));

$this->start('topmenu');
echo $this->Html->link('Home', array('controller' => 'handset'), array('class' => 'easyui-linkbutton', 'iconCls' => 'icon-16-home')) . PHP_EOL;
$this->end();
// We're only going to display the sample/instructions when nothing has been
// uploaded.
if (empty ($results)) {
?>
<p>Upload a spreadsheet or CSV file. The columns must be formatted as follows:</p>
<table class="simple" style="width: 550px;">
<thead>
    <tr>
        <th>MAC Address</th>
        <th>TN</th>
        <th><em>Campus Code *</em></th>
    </tr>
    </thead>
    <tbody>
    <tr>
    <td>00:00:00:00:00:00</td>
    <td>00-00-00-00</td>
    <td>0</td>
    </tr>
    <tr>
    <td>00:00:00:00:00:00</td>
    <td>00-00-00-00</td>
    <td>0</td>
    </tr>
    <tr>
    <td>00:00:00:00:00:00</td>
    <td>00-00-00-00</td>
    <td>0</td>
    </tr>
    </tbody>
</table>
<p><em>* Optional</em></p>
<?php
}
?>
<div class="fileUpload">
<h3>Upload</h3>
<?php
echo $this->Form->create('Upload', array(
    'type' => 'file'
));
echo $this->Form->file('File');
echo $this->Form->end('Upload');
?>
</div>
<?php
if (!empty ($worksheetnames)) {
    echo '<h3>Worksheet Names</h3>' . PHP_EOL;
    echo '<ol>' . PHP_EOL;
    foreach ($worksheetnames as $wn) {
        echo '<li>', $wn, '</li>' . PHP_EOL;
    }
    echo '</ol>' . PHP_EOL;
}
if (!empty ($results)) {
    ?>
    <h3>Worksheet Data</h3>
    <table class="cake-generic">
        <thead>
            <tr>
            <th>MAC Address</th>
            <th>TN</th>
            <th>Campus</th>
            <th>Status</th>
            <th>Action</th>
            </tr>
        </thead>
        <tbody>
        <?php
        foreach ($results as $row) {
            if ($row['is_valid'] == true) {
                $valid_msg = "Valid";
            } else {
                $valid_msg = "Invalid";
            }
            ?>
            <tr>
                <td><?php echo $row['mac']; ?></td>
                <td><?php echo $row['tn']; ?></td>
                <td><?php echo $row['config_type']; ?></td>
                <td><?php echo $valid_msg; ?></td>
                <td><?php echo $row['action']; ?></td>
            </tr>
        <?php
        }
        ?>
        </tbody>
    </table>
<?php
}
?>
