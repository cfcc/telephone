<?php
App::uses('AppController', 'Controller');
/**
 * LogRequests Controller
 *
 * @property LogRequest $LogRequest
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class LogRequestsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->LogRequest->recursive = 0;
        $this->set('logRequests', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
        $this->layout = 'cake_default';
		if (!$this->LogRequest->exists($id)) {
			throw new NotFoundException(__('Invalid log request'));
		}
		$options = array('conditions' => array('LogRequest.' . $this->LogRequest->primaryKey => $id));
		$this->set('logRequest', $this->LogRequest->find('first', $options));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
        $this->layout = 'cake_default';
		$this->LogRequest->id = $id;
		if (!$this->LogRequest->exists()) {
			throw new NotFoundException(__('Invalid log request'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->LogRequest->delete()) {
			$this->Flash->success(__('The log request has been deleted.'));
		} else {
			$this->Flash->error(__('The log request could not be deleted. Please, try again.'));
		}
		$this->redirect(array('action' => 'index'));
	}
}
