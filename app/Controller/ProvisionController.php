<?php
define ("SYSTEM_CONFIG_TYPE", "100");

/**
 * Class ProvisionController
 *
 * This builds text files based on the database records for the phones to download.
 *
 * See http://pbxbook.com/voip/autoprov.html for more details on how Avaya auto-provision works.
 *
 * See http://pbxbook.com/voip/sipunifw.html for details on upgrading firmware on the handsets.
 *
 */
class ProvisionController extends AppController
{
    public $uses = array(
        'Handset',
        'Settings',
        'LogRequest'
    );

    public function beforeFilter() {
        parent::beforeFilter();
        // The provision controller does not require login, so allow everything
        $this->Auth->allow();
    }

    /**
     * Get the Configuration values from the database and return an array.
     *
     * @param $config_type
     * @return array
     */
    function get_config($config_type)
    {
        if ($config_type == SYSTEM_CONFIG_TYPE) {
            $sc_records = $this->Settings->find('all', array(
                'conditions' => array('Settings.config_type_id' => $config_type)));
        } else {
            $sc_records = $this->Settings->find('all', array(
                'conditions' => array(
                    "OR" => array(
                        array('Settings.config_type_id' => $config_type),
                        array('Settings.config_type_id' => 0)
                    )),
                'order' => array('Settings.config_type_id', 'Settings.config_code')
            ));
        }
        // Rebuild the settings as a hash of the config codes so that campus-specific settings will override the default
        //  setting, which is why we order the select by config_type_id above.
        $settings = array();
        foreach ($sc_records as $rec) {
            $settings[$rec['Settings']['config_code']] = array(
                'config_value' => $rec['Settings']['config_value'],
                'comment' => $rec['Settings']['comment']
            );
        }
        return $settings;
    }

    /**
     * Convert the configuration array to a string suitable for export as a text file.
     *
     * @param $settings
     * @return string
     */
    function get_config_as_text($settings) {
        $content = "";
        foreach ($settings as $config_key => $rec) {
            // TODO: find out why the comments are not working, should the spaces be tabs?
            // $content .= $config_key . "=" . $rec['config_value'] . ";    # " . $rec['comment'] . PHP_EOL;
            $content .= $config_key . "=" . $rec['config_value'] . ";" . PHP_EOL;
        }
        $content .= PHP_EOL;
        return $content;
    }

    /**
     * Return the configuration settings as a text file or raise a "404 Not Found" exception.
     *
     * @return CakeResponse
     */
    public function get()
    {
        $user_agent = $this->request->header('User-Agent');
        $client_ip = $this->request->clientIp();

        $url = $this->request->here;
        $last_slash = strrpos($url, "/") + 1;
        $last_period = strrpos($url, ".");
        $fn_length = $last_period - $last_slash;
        $filename = substr($url, $last_slash, $fn_length);
        $extension = substr($url, $last_period + 1);

        $content = "";

        $handset_id = null;

        $this->log("Provision file '$filename' requested by '$user_agent' on '$client_ip'.", 'debug');

        if ($filename == "SYSTEM" || $filename == "system" || $filename == "100") {
            $content .= $this->get_config_as_text($this->get_config(SYSTEM_CONFIG_TYPE));
        } else {
            if ($extension == "prv") {
                if (!empty ($filename)) {
                    $hs_record = $this->Handset->find('first', array(
                        'conditions' => array(
                            'Handset.mac_address' => $filename
                        )
                    ));
                    if (!empty ($hs_record)) {
                        # preserve the handset_id for the log
                        $handset_id = $hs_record['Handset']['id'];
                        $mac_addr = substr($hs_record['Handset']['mac_address'], 0, 2);
                        for ($i = 2; $i < strlen($hs_record['Handset']['mac_address']); $i += 2) {
                            $mac_addr .= ":" . substr($hs_record['Handset']['mac_address'], $i, 2);
                        }
                        $content .= $this->get_config_as_text($this->get_config($hs_record ['Handset'] ['config_type_id']));
                        $content .= "reg=" . $mac_addr . "," . $hs_record ['Handset'] ['call_server_type'] . "," . $hs_record ['Handset'] ['connect_server'] . "," . $hs_record ['Handset'] ['node_id'] . "," . $hs_record ['Handset'] ['terminal_number'] . ";" . PHP_EOL;
                    } else {
                        throw new NotFoundException ('MAC address not found: ' . $filename);
                    }
                }
            } elseif ($extension == "cfg") {
                if ($filename == "12xxBoot" || $filename == "1220" || $filename == "1200") {
                    $config = $this->get_config("1200");
                }
                elseif ($filename == "1140e" || $filename == "1100") {
                    $config = $this->get_config("1100");
                } else {
                    throw new NotFoundException ('Configuration file not found');
                }
                $content = "[FW]" . PHP_EOL;
                $content .= "DOWNLOAD_MODE " . $config['DOWNLOAD_MODE']['config_value'] . PHP_EOL;
                $content .= "VERSION " . $config['VERSION']['config_value'] . PHP_EOL;
                $content .= "FILENAME " . $config['VERSION']['config_value'] . ".bin" . PHP_EOL;
                $content .= "PROTOCOL " . $config['PROTOCOL']['config_value'] . PHP_EOL;
                $content .= "SERVER_IP " . $config['SERVER_IP']['config_value'] . PHP_EOL;
                $content .= "SECURITY_MODE " . $config['SECURITY_MODE']['config_value'] . PHP_EOL;
            } else {
                throw new NotFoundException ('File extension not recognized: ' . $extension);
            }
        }

        # we are only going to log requests made by phones, identified by the lack of a user agent string
        if (empty($user_agent)) {
            $this->LogRequest->create();
            $log_data = array(
                'LogRequest' => array(
                    'filename' => $filename,
                    'ip_address' => ip2long($client_ip),
                    'handset_id' => $handset_id
                )
            );
            $this->LogRequest->save($log_data);
        }
        $this->response->body($content);
        $this->response->type('text/plain');

        return $this->response;
    }
}
