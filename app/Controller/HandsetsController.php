<?php
App::uses('AppController', 'Controller');
/**
 * Handsets Controller
 *
 * @property Handset $Handset
 * @property PaginatorComponent $Paginator
 * @property RequestHandlerComponent $RequestHandler
 * @property SessionComponent $Session
 */
class HandsetsController extends AppController {

    const CONFIG_LIMIT = '100';

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'RequestHandler', 'Session');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Handset->recursive = 1;
        $this->Handset->order = array('Handset.extension' => 'asc', 'Handset.mac_address' => 'asc');
		$this->set('handsets', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Handset->exists($id)) {
			throw new NotFoundException(__('Invalid handset'));
		}
		$options = array('conditions' => array('Handset.' . $this->Handset->primaryKey => $id));
		$this->set('handset', $this->Handset->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Handset->create();

            // record the user who created this record
            $this->request->data['created_by'] = $this->Auth->user('username');
            // also put them as the user who first modified the record
            $this->request->data['modified_by'] = $this->Auth->user('username');
            // fix the MAC address so it is all upper case
            $this->request->data['mac_address'] = strtoupper($this->request->data['mac_address']);

            try {
                $result = $this->Handset->save($this->request->data);
                if ($result) {
                    $this->Flash->set(__('The handset has been saved.'));
                    $this->redirect(array('action' => 'index'));
                } else {
                    $this->Flash->set(__('The handset could not be saved. Please, try again.'));
                }
            } catch (Exception $exc) {
                $this->Flash->set(__('Error saving handset: ') . $exc);
            }
		}
		$nodes = $this->Handset->Node->find('list');
        $conditions = array('ConfigType.id <' => self::CONFIG_LIMIT);
		$configTypes = $this->Handset->ConfigType->find('list', array('conditions' => $conditions));
		$this->set(compact('nodes', 'configTypes'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Handset->exists($id)) {
			throw new NotFoundException(__('Invalid handset'));
		}
		if ($this->request->is(array('post', 'put'))) {
            // record the user who modified this record
            $this->request->data['modified_by'] = $this->Auth->user('username');
            // fix the MAC address so it is all upper case
            $this->request->data['mac_address'] = strtoupper($this->request->data['mac_address']);

			if ($this->Handset->save($this->request->data)) {
				$this->Flash->success(__('The handset has been saved.'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The handset could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Handset.' . $this->Handset->primaryKey => $id));
			$this->request->data = $this->Handset->find('first', $options);
		}
		$nodes = $this->Handset->Node->find('list');
        $conditions = array('ConfigType.id <' => self::CONFIG_LIMIT);
		$configTypes = $this->Handset->ConfigType->find('list', array('conditions' => $conditions));
		$this->set(compact('nodes', 'configTypes'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Handset->id = $id;
		if (!$this->Handset->exists()) {
			throw new NotFoundException(__('Invalid handset'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Handset->delete()) {
			$this->Session->setFlash(__('The handset has been deleted.'));
		} else {
			$this->Session->setFlash(__('The handset could not be deleted. Please, try again.'));
		}
		$this->redirect(array('action' => 'index'));
	}

    /**
     * send a JSON response with a list of phones
     */
    public function get()
    {
        $rows = array();
        $result = $this->Handset->find('all', array('order' => array('Handset.mac_address')));
        foreach ($result as $line) {
            if (array_key_exists('LogRequest', $line)) {
                if (count($line['LogRequest']) > 0) {
                    $line['Handset']['last_ip'] = long2ip($line['LogRequest'][0]['ip_address']);
                }
            }
            $rows [] = $line ['Handset'];
        }
        $this->set('rows', $rows);
        $this->set('_serialize', array(
            'rows'
        ));
    }
}
