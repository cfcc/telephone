<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {
    public $components = array(
        'Auth' => array(
            'loginRedirect' => array(
                'controller' => 'Handsets',
                'action' => 'index'
            ),
            'logoutRedirect' => array(
                'controller' => 'users',
                'action' => 'login'
            ),
            'authenticate' => array(
                'YALP.LDAP' => null
            ),
        ),
        'Session',
        'Flash'
    );

    public $helpers = array('Html', 'Form', 'Session', 'Flash');

    /**
     * Called before the controller action.
     *
     * @return void
     * @link http://book.cakephp.org/2.0/en/controllers.html#request-life-cycle-callbacks
     */
    function beforeFilter() {
        // If YALP not loaded then use Form Auth
        if (CakePlugin::loaded('YALP')) {
            $this->Auth->authenticate = array('YALP.LDAP' => null);
        } else {
            debug("YALP.LDAP not loaded");
        }
        $this->Auth->authError = __('Please log in with your CFCC username and password.');
        // Default access to controller actions
        $this->Auth->allow('get');

        parent::beforeFilter();
    }
}
