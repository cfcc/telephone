<?php
App::uses('AppController', 'Controller');
/**
 * ConfigTypes Controller
 *
 * @property ConfigType $ConfigType
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class ConfigTypesController extends AppController {

/**
 * Components
 *
 * @var array
 */
    public $helpers = array('Flash');
	public $components = array('Paginator', 'Session', 'Flash');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->ConfigType->recursive = 0;
		$this->set('configTypes', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->ConfigType->exists($id)) {
			throw new NotFoundException(__('Invalid config type'));
		}
		$options = array('conditions' => array('ConfigType.' . $this->ConfigType->primaryKey => $id));
		$this->set('configType', $this->ConfigType->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->ConfigType->create();
			try {
                if ($this->ConfigType->save($this->request->data)) {
                    $this->Flash->success(__('The config type has been saved.'));
                    $this->redirect(array('action' => 'index'));
                    return;
                } else {
                    $this->Flash->error(__('The config type could not be saved. Please, try again.'));
                }
            } catch (Exception $e) {
			    $this->Flash->error(__('The config type could not be saved: ' . $e->getMessage()));
            }
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->ConfigType->exists($id)) {
			throw new NotFoundException(__('Invalid config type'));
		}
		if ($this->request->is(array('post', 'put'))) {
		    try {
                if ($this->ConfigType->save($this->request->data)) {
                    $this->Flash->success(__('The config type has been saved.'));
                    $this->redirect(array('action' => 'index'));
                    return;
                } else {
                    $this->Flash->error(__('The config type could not be saved. Please, try again.'));
                }
            } catch (Exception $e) {
		        $this->Flash->error(__('The config type has not been saved: ' . $e->getMessage()));
            }
		} else {
			$options = array('conditions' => array('ConfigType.' . $this->ConfigType->primaryKey => $id));
			$this->request->data = $this->ConfigType->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->ConfigType->id = $id;
		if (!$this->ConfigType->exists()) {
			throw new NotFoundException(__('Invalid config type'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->ConfigType->delete()) {
			$this->Flash->success(__('The config type has been deleted.'));
		} else {
			$this->Flash->error(__('The config type could not be deleted. Please, try again.'));
		}
		$this->redirect(array('action' => 'index'));
        return;
	}
}
