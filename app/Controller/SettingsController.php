<?php
App::uses('AppController', 'Controller');
/**
 * Settings Controller
 *
 * @property Setting $Setting
 * @property PaginatorComponent $Paginator
 * @property RequestHandlerComponent $RequestHandler
 * @property SessionComponent $Session
 */
class SettingsController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator', 'RequestHandler', 'Session');

    /**
     * index method
     *
     * @return void
     */
    public function index() {
        $this->Setting->recursive = 0;
        $this->set('settings', $this->Paginator->paginate());
    }

    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($id = null) {
        if (!$this->Setting->exists($id)) {
            throw new NotFoundException(__('Invalid setting'));
        }
        $options = array('conditions' => array('Setting.' . $this->Setting->primaryKey => $id));
        $this->set('setting', $this->Setting->find('first', $options));
    }

    /**
     * add method
     *
     * @return void
     */
    public function add() {
        if ($this->request->is('post')) {
            $this->Setting->create();
            try {
                if ($this->Setting->save($this->request->data)) {
                    $this->Flash->success(__('The setting has been saved.'));
                    $this->redirect(array('action' => 'index'));
                    return;
                } else {
                    $this->Flash->error(__('The setting could not be saved. Please, try again.'));
                }
            } catch (Exception $e) {
                $this->Flash->error(__('The setting could not be saved. Please, try again. ' . $e->getMessage() ));
            }
        }
        $configTypes = $this->Setting->ConfigType->find('list');
        $this->set(compact('configTypes'));
    }

    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit($id = null) {
        if (!$this->Setting->exists($id)) {
            throw new NotFoundException(__('Invalid setting'));
        }
        if ($this->request->is(array('post', 'put'))) {
            try {
                if ($this->Setting->save($this->request->data)) {
                    $this->Flash->success(__('The setting has been saved.'));
                    $this->redirect(array('action' => 'index'));
                    return;
                } else {
                    $this->Flash->error(__('The setting could not be saved. Please, try again.'));
                }
            } catch (Exception $e) {
                $this->Flash->error(__('The setting could not be saved. Please, try again. ' . $e->getMessage()));
            }
        } else {
            $options = array('conditions' => array('Setting.' . $this->Setting->primaryKey => $id));
            $this->request->data = $this->Setting->find('first', $options);
        }
        $configTypes = $this->Setting->ConfigType->find('list');
        $this->set(compact('configTypes'));
    }

    /**
     * delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function delete($id = null) {
        $this->Setting->id = $id;
        if (!$this->Setting->exists()) {
            throw new NotFoundException(__('Invalid setting'));
        }
        $this->request->allowMethod('post', 'delete');
        if ($this->Setting->delete()) {
            $this->Flash->success(__('The setting has been deleted.'));
        } else {
            $this->Flash->error(__('The setting could not be deleted. Please, try again.'));
        }
        $this->redirect(array('action' => 'index'));
        return;
    }

    public function get()
    {
        // send the list of configuration values as a JSON response
        $rows = array();
        $result = $this->Setting->find('all', array(
            'order' => array(
                'Settings.config_type_id',
                'Settings.config_code'
            )
        ));
        foreach ($result as $line) {
            $rows [] = $line ['Setting'];
        }
        $this->set('rows', $rows);
        $this->set('_serialize', array(
            'rows'
        ));
    }

    public function save()
    {
        if ($this->request->is('post')) {
            // If this is a new record, record the user who created it, otherwise mark it as modified.
            if (!$this->request->data['id']) {
                $this->request->data['created_by'] = $this->Auth->user('username');
            } else {
                $this->request->data['modified_by'] = $this->Auth->user('username');
            }
            if ($this->Settings->save($this->request->data)) {
                // retrieve the record we just saved
                $result = $this->Settings->findById($this->Settings->id);
                $this->set('rows', $result ['Settings']);
                $this->set('_serialize', array(
                    'rows'
                ));
            } else {
                $this->set('errorMsg', 'Save failed');
                $this->set('_serialize', array(
                    'errorMsg'
                ));
            }
        }
    }
}

