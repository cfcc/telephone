<?php
App::import('Vendor', 'PHPExcel', array(
    'file' => 'PHPExcel' . DS . 'PHPExcel.php'
));
if (!class_exists('PHPExcel')) {
    throw new CakeException ('Vendor class PHPExcel not found!');
}

define ('RE_MAC_ADDR', "/(\w\w*\:){5}(\w*\w)/");
define ('RE_TN', "/(\d+[: -]){3}(\d+)/");

class UploadController extends AppController
{
    public $uses = array(
        'Handset'
    );

    public function index()
    {
        if (!empty ($this->request->data) && is_uploaded_file($this->request->data['Upload']['File']['tmp_name'])) {
            // $objReader = PHPExcel_IOFactory::load ( $this->request->data ['Upload'] ['File'] ['tmp_name'] );
            $inputFileName = $this->request->data['Upload']['File']['tmp_name'];
            $objPHPExcel = PHPExcel_IOFactory::load($inputFileName);
            $active_sheet = $objPHPExcel->getActiveSheet();

            $results = array();
            foreach ($active_sheet->getRowIterator() as $row) {
                $line_result = array(
                    'mac' => '',
                    'tn' => '',
                    'config_type' => 0,
                    'is_valid' => true,
                    'action' => ''
                );
                $cell_iterator = $row->getCellIterator();
                foreach ($cell_iterator as $cell) {
                    if ($cell->getColumn() == 'A') {
                        $value = $cell->getValue();
                        if (preg_match(RE_MAC_ADDR, $value)) {
                            $line_result ['mac'] = strtoupper(str_replace(array(
                                ":",
                                "-",
                                " "
                            ), "", $value));
                        } else {
                            $line_result ['mac'] = $value;
                            $line_result ['is_valid'] = false;
                        }
                    } elseif ($cell->getColumn() == 'B') {
                        $value = $cell->getValue();
                        if (preg_match(RE_TN, $value)) {
                            $line_result ['tn'] = $value;
                        } else {
                            $line_result ['tn'] = $value;
                            $line_result ['is_valid'] = false;
                        }
                    } elseif ($cell->getColumn() == 'C') {
                        // the config_type is optional
                        $value = intval($cell->getValue());
                        $line_result['config_type'] = $value;
                    }
                }
                // If this row is valid, then we can save it to the database
                if ($line_result ['is_valid'] == true) {
                    // It's okay to save, build the record that we will store
                    $record = array(
                        'Handset' => array(
                            'mac_address' => $line_result ['mac'],
                            'call_server_type' => 'CS1K',
                            'connect_server' => 'S1S2',
                            'node_id' => '0001',
                            'terminal_number' => $line_result ['tn'],
                            'config_type_id' => $line_result['config_type'],
                        )
                    );
                    // See if we have an existing record to update
                    $result = $this->Handset->find('first', array(
                        'conditions' => array(
                            'Handset.mac_address' => $line_result ['mac']
                        )
                    ));
                    if (!empty ($result)) {
                        $record['Handset']['modified_by'] = $this->Auth->user('id');
                        $this->Handset->read(null, $result['Handset']['id']);
                        $this->Handset->set($record);
                        $this->Handset->save();
                        $line_result ['action'] = 'Updated';
                    } else {
                        $record['Handset']['created_by'] = $this->Auth->user('id');
                        $this->Handset->create();
                        $this->Handset->save($record);
                        $line_result ['action'] = 'Added';
                    }
                    $this->Handset->clear();
                }
                $results [] = $line_result;
            }
            $this->set('results', $results);
        }
    }
}
