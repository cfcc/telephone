/*
 * Code for the app/View/Settings/index.ctp page
 */
// the base URL variable is set in the easyui template
var base_url = "";
if (typeof cake_base_url != 'undefined') { base_url = cake_base_url; }
var url;
function newSetting(){
    $('#dlg').dialog('open').dialog('setTitle','New Setting');
    $('#fm').form('clear');
    url = base_url + '/settings/save.json';
}
function editSetting(){
    var row = $('#dg').datagrid('getSelected');
    if (row){
        $('#dlg').dialog('open').dialog('setTitle','Edit Setting');
        $('#fm').form('load',row);
        url = base_url + '/settings/save.json';
    }
}
function saveSetting(){
    $('#fm').form('submit',{
        url: url,
        onSubmit: function(){
            return $(this).form('validate');
        },
        success: function(result){
            var result = eval('('+result+')');
            if (result.errorMsg){
                $.messager.show({
                    title: 'Error',
                    msg: result.errorMsg
                });
            } else {
                $('#dlg').dialog('close');     // close the dialog
                $('#dg').datagrid('reload');   // reload the user data
            }
        }
    });
}
function deleteSetting(){
    var row = $('#dg').datagrid('getSelected');
    if (row){
        $.messager.confirm('Confirm','Are you sure you want to delete this setting?',function(r){
            if (r){
                $.post(base_url + '/settings/delete',{id:row.id},function(result){
                    if (result.success){
                            $('#dg').datagrid('reload');    // reload the user data
                    } else {
                            $.messager.show({       // show error message
                                    title: 'Error',
                                    msg: result.errorMsg
                            });
                    }
                },'json');
            }
        });
    }
}
function previewSetting(){
    var row = $('#dg').datagrid('getSelected');
    if (row) {
        var config_filenames = { 100: "SYSTEM.PRV", 1100: "1140e.cfg", 1200: "1220.cfg"};
        var filename = config_filenames[row.config_type_id]; // ajax request to get the filename base on the config type ID
        if (typeof filename != 'undefined') {
            location.href = base_url + '/provision/get/'+filename;
        } else {
            $.messager.show({
                title: 'Info',
                msg: 'Nothing has been defined for that config type'
            });
        }
    } else {
        $.messager.show({
            title: 'Info',
            msg: 'You must select a row first'
        });
    }
    return false;
}
