/*
 * Code for the listing page
 */
// the base URL variable is set in the easyui template
var base_url = "";
if (typeof cake_base_url != 'undefined') { base_url = cake_base_url; }
var url;
function newEntry(){
    $('#dlg').dialog('open').dialog('setTitle','New Entry');
    $('#fm').form('clear');
    // set default fields
    $('#fm').form('load',{call_server_type:'CS1K',connect_server:'S1S2',node_id:'0001'});
    url = base_url + '/handset/save.json';
}
function editEntry(){
    var row = $('#dg').datagrid('getSelected');
    if (row){
        $('#dlg').dialog('open').dialog('setTitle','Edit Entry');
        $('#fm').form('load',row);
        url = base_url + '/handset/save.json';
    }
}
function saveEntry(){
    $('#fm').form('submit',{
        url: url,
        onSubmit: function(){
            return $(this).form('validate');
        },
        success: function(result){
            var result = eval('('+result+')');
            if (result.errorMsg){
                $.messager.show({
                    title: 'Error',
                    msg: result.errorMsg
                });
            } else {
                $('#dlg').dialog('close');     // close the dialog
                $('#dg').datagrid('reload');   // reload the user data
            }
        }
    });
}
function destroyEntry(){
    var row = $('#dg').datagrid('getSelected');
    if (row){
        $.messager.confirm('Confirm','Are you sure you want to destroy this entry?',function(r){
            if (r){
                $.post(base_url + '/handset/delete',{id:row.id},function(result){
                    if (result.success){
                            $('#dg').datagrid('reload');    // reload the user data
                    } else {
                            $.messager.show({       // show error message
                                    title: 'Error',
                                    msg: result.errorMsg
                            });
                    }
                },'json');
            }
        });
    }
}
function previewEntry(){
	var row = $('#dg').datagrid('getSelected');
	if (row){
		location.href = base_url + '/provision/get/'+row.mac_address+".prv";
	} else {
		$.messager.show({
			title: 'Info',
			msg: 'You must select a row first'
		});
	}
    return false;
}
